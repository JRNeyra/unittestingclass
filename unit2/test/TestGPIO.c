
#include "unity.h"
#include "GPIO.h"
#include "MK20DX256.h"

//Global variable for testing
#define GPIO_OK 0


//Requirement_1_Testing
//=========================OUTPUT_TESTS================================
// HAPPY DAY TESTING
//=====================================================================
void test_SetPinAsOutput_should_ConfigurePinDirection(void) {
    PORTC.PDDR = 0;						//Configures all bits as inputs (0's)

    TEST_ASSERT_EQUAL(GPIO_OK, GPIO_SetPinAsOutput(0));
    TEST_ASSERT_EQUAL_HEX32(BIT_TO_MASK(0), PORTC.PDDR);

    TEST_ASSERT_EQUAL(GPIO_OK, GPIO_SetPinAsOutput(22));
    TEST_ASSERT_EQUAL_HEX32(BIT_TO_MASK(0) | BIT_TO_MASK(22), PORTC.PDDR);

    TEST_ASSERT_EQUAL(GPIO_OK, GPIO_SetPinAsOutput(31));
    TEST_ASSERT_EQUAL_HEX32(BIT_TO_MASK(0) | BIT_TO_MASK(22) | BIT_TO_MASK(31), 
    	PORTC.PDDR);
}

//=====================================================================
// LIMITS TESTING
//=====================================================================
void test_SetPinAsOutput_should_NotUpdatePinDirection_when_PinIsNotValid(void){
	PORTC.PDDR = 0;

	TEST_ASSERT_NOT_EQUAL(0, GPIO_SetPinAsOutput(32));	//First value that should fail 
	TEST_ASSERT_EQUAL_HEX32(0, PORTC.PDDR);
}
//=====================================================================


//==========================INPUT_TESTS================================
// HAPPY DAY TESTING
//=====================================================================
void test_SetPinAsInput_should_ConfigurePinDirection(void) {
    PORTC.PDDR = 0xFFFFFFFF;			//Configures all bits as outputs (1's)

    TEST_ASSERT_EQUAL(GPIO_OK, GPIO_SetPinAsInput(0));
    TEST_ASSERT_EQUAL_HEX32( ~(BIT_TO_MASK(0)), PORTC.PDDR);	//Adding bit wise inversion to switch to low  

    TEST_ASSERT_EQUAL(GPIO_OK, GPIO_SetPinAsInput(22));
    TEST_ASSERT_EQUAL_HEX32( ~(BIT_TO_MASK(0) | BIT_TO_MASK(22)), PORTC.PDDR);

    TEST_ASSERT_EQUAL(GPIO_OK, GPIO_SetPinAsInput(31));
    TEST_ASSERT_EQUAL_HEX32( ~(BIT_TO_MASK(0) | BIT_TO_MASK(22) | BIT_TO_MASK(31)), 
    	PORTC.PDDR);
}

//=====================================================================
// LIMITS TESTING
//=====================================================================
void test_SetPinAsInput_should_NotUpdatePinDirection_when_PinIsNotValid(void){
	PORTC.PDDR = 0xFFFFFFFF;			//Configures all bits as outputs (1's)

	TEST_ASSERT_NOT_EQUAL(0, GPIO_SetPinAsInput(32));	//First value that should fail 
	TEST_ASSERT_EQUAL_HEX32(0xFFFFFFFF, PORTC.PDDR);
}
//=====================================================================



//Requirement_2_Testing


//=====================================================================
//	MAIN FUNCTION
//=====================================================================
int main(void) {
    UNITY_BEGIN();

    // Output Testing
    RUN_TEST(test_SetPinAsOutput_should_ConfigurePinDirection);
    RUN_TEST(test_SetPinAsOutput_should_NotUpdatePinDirection_when_PinIsNotValid);
    
    //Input Testing
    RUN_TEST(test_SetPinAsInput_should_ConfigurePinDirection);
    RUN_TEST(test_SetPinAsInput_should_NotUpdatePinDirection_when_PinIsNotValid);

    return UNITY_END();
}
