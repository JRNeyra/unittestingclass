#ifndef GPIO_H
#define GPIO_H

#include <stdint.h>						//Allows the use of explicit data types like uint8_t

//Requirement 1: Each Pin can be set as outputs or inputs
int GPIO_SetPinAsOutput(uint8_t Pin);
int GPIO_SetPinAsInput(uint8_t Pin);

//Requirement 2: Each Pin can be set high or low
int GPIO_SetPin(uint8_t Pin);
int GPIO_ClearPin(uint8_t Pin);


#endif //GPIO_H
