#include "GPIO.h"
#include "MK20DX256.h"

//=======================================================
//Requirement 1: Each Pin can be set as outputs or inputs  
int GPIO_SetPinAsOutput(uint8_t Pin){
	if(Pin >= 32)			//Fail if the Pin number trying to be changed is higher than 31
		return 1;

	PORTC.PDDR |= BIT_TO_MASK(Pin);
	return 0;
}

int GPIO_SetPinAsInput(uint8_t Pin){
	if(Pin >= 32)			//Fail if the Pin number trying to be changed is higher than 31
		return 1;

	PORTC.PDDR &= ~(BIT_TO_MASK(Pin));		//Changed the OR to AND and bitwise inversion to flip all bits to leave the one off
	return 0;
}
//=====================================================


//=====================================================
//Requirement 2: Each Pin can be set high or low
int GPIO_SetPin(uint8_t Pin) {

	return 1;
}

int GPIO_ClearPin(uint8_t Pin) {

	return 1;
}
//=====================================================
