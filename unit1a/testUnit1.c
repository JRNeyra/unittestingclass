#include "unity.h"

/*******.......Tests.......*******/
void test_TheFirst(void) {
    //TEST_IGNORE_MESSAGE("Implement Me");				//Used to ignore test and print message
    //TEST_ASSERT(1>2);									//Compares values (First item is expectation and second is actual item)
	//TEST_ASSERT_MESSAGE(1==1, "1 did not equal 2");	//Compares values and allows for message when test fails
	TEST_ASSERT_EQUAL(1,2);								//Compares the values and provides automatic explanation when false/failure
}	

void test_TheSecond(void) {
	//TEST_ASSERT_TRUE(10 < 20);						//Passes if the statement is true
	TEST_ASSERT_FALSE(8 > 7.9);							//Passes if the statement is false
}

void test_TheThird(void) {
	TEST_ASSERT_EQUAL_FLOAT(1.2, 1.3);
}

void test_TheFourth(void) {
	TEST_ASSERT_EQUAL_INT32(-1, 0xFFFFFFFF);			//INT32 allows to check 32 bit hex values
}


/*******.......MAIN FUNCTION.......*******/
int main(void) {
    UNITY_BEGIN();										//Must use this function to initialize UNITY
    
    RUN_TEST(test_TheFirst);
    RUN_TEST(test_TheSecond);
    RUN_TEST(test_TheThird);
    RUN_TEST(test_TheFourth);

    return UNITY_END();									//Must use this function to end UNITY
}
