#include "unity.h"
#include "Fib.h"
#include <limits.h>

#define DIMENSION_OF(a) (sizeof(a)/sizeof(a[0]))							//Macro to get the number of elements in array


//Global testing variable
int resulted_value;


/************............TESTS............************/

///////*******First_Function_Tests*******///////
/*******NORMAL_OPERATION_TESTING*******/
//First Test
void test_element0_should_return1(void) {
    //TEST_IGNORE_MESSAGE("Implement Me");
    resulted_value = Fibonacci_GetElement(0);
    TEST_ASSERT_EQUAL_INT(1, resulted_value);
}

//Second Test
void test_element1_should_return1(void) {
    resulted_value = Fibonacci_GetElement(1);
    TEST_ASSERT_EQUAL_INT(1, resulted_value);
}

//Third Test
void test_element2_should_return2(void) {
    resulted_value = Fibonacci_GetElement(2);
    TEST_ASSERT_EQUAL_INT(2, resulted_value);
}

//Multiple Elements Test
void test_part_of_the_sequence(void) {
	int expected_results[] = {1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144};		//Fibonacci elemnets to test
	for (int i = 0; i < DIMENSION_OF(expected_results); i++) {
		TEST_ASSERT_EQUAL_INT(expected_results[i], Fibonacci_GetElement(i));
	}
}

/*******LIMITS_TESTING*******/
void test_that_negative_elements_return0(void) {
	TEST_ASSERT_EQUAL_INT(0, Fibonacci_GetElement(-1));
	TEST_ASSERT_EQUAL_INT(0, Fibonacci_GetElement(-3));
	TEST_ASSERT_EQUAL_INT(0, Fibonacci_GetElement(-555));
	TEST_ASSERT_EQUAL_INT(0, Fibonacci_GetElement(INT_MIN));					//Lowest int value, needs #include <limits.h>
}

void test_that_overrun_values_return0(void) {
	const int first_bad_element = 46;											//First array element that overruns the int datatype value
	const int last_good_element = first_bad_element - 1;						//Last array element that exists in int datatype
	int last_good_value = Fibonacci_GetElement(last_good_element);				//Value from Fibonacci series that corresponds to the last good element in array

	TEST_ASSERT_EQUAL_INT_MESSAGE(4, sizeof(int), 
		"Constants first_bad_element and FIB_MAX_ELEMENT Must Be Changed");		//Initial test to check if int is 4 bytes for right enviroment architecture
	
	TEST_ASSERT_MESSAGE(last_good_value > 1, "This value should not have been rollover");
	TEST_ASSERT_EQUAL_INT(0, Fibonacci_GetElement(first_bad_element));
	TEST_ASSERT_EQUAL_INT(0, Fibonacci_GetElement(INT_MAX));					//Maximun int datatype value
}


///////*******Second_Function_Tests*******///////
void test_given_number_exists(void){
	int given_number;
	printf("Enter number to test:\n");
	scanf("%d", &given_number);
	int returned_value = Fibonacci_IsInSequence(given_number);

	TEST_ASSERT_EQUAL_INT_MESSAGE(1, returned_value, 
		"The given number is not a Fibonacci number");
}



/*******.......MAIN_FUNCTION......*******/
int main(void) {
    UNITY_BEGIN();

    ///***First_Function_Tests***///
    //Normal operation testing
    RUN_TEST(test_element0_should_return1);
    RUN_TEST(test_element1_should_return1);
    RUN_TEST(test_element2_should_return2);
    RUN_TEST(test_part_of_the_sequence);

    //Limits testing
    RUN_TEST(test_that_negative_elements_return0);
    RUN_TEST(test_that_overrun_values_return0);
    

    ///***Second_Function_Tests***///
    RUN_TEST(test_given_number_exists);

    return UNITY_END();
}
